


import React from "react";
import {useEffect} from "react";


function ShowApi() {

    useEffect(() => {

        setTimeout(() => {

            const url = `https://jsonplaceholder.typicode.com/todos/`;

            fetch(url, {method: 'GET'}
            ).then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('Something went wrong.');
            })
                .then((responseJson) => {

                    let myTable = document.querySelector('#tbody');
                    let table = document.createElement('table');

                    responseJson.forEach(items => {

                        let row = document.createElement('tr');

                        Object.values(items).forEach(text => {

                            let cell = document.createElement('td');
                            let textNode = document.createTextNode(text);

                            cell.appendChild(textNode);
                            row.appendChild(cell)
                        })

                        table.appendChild(row);
                    })

                    myTable.appendChild(table);

                }).catch((error) => {
                console.log(error + ", or Please check your internet connection and try again.");
            })
        }, 3000);
    }, []);

    return (
        <div id="tbody"></div>
    )
}

export default ShowApi;

